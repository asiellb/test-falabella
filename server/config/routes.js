import { Router } from 'express';

import index from '../components/index/index.api';
import albums from '../components/albums/albums.api';

const router = Router();

router.use('/', index);
router.use('/albums', albums);

export default router;