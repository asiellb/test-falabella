import mongoose from 'mongoose';
import bluebird from 'bluebird';

const dbURL = process.env.DB_HOST;
mongoose.Promise = bluebird;
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
mongoose.connect(dbURL, { useNewUrlParser: true });
var conn = mongoose.connection;
conn.on('error', (err) => {
    console.error('There was a db connection error');
    return console.error(err.message);
});
conn.on('connected', () => {
    if (mongoose.connection.client.s.url.startsWith('mongodb+srv')) {
        mongoose.connection.db = mongoose.connection.client.db(process.env.DB_NAME);
    }
    return console.log('Successfully connected to ' + dbURL);
});
conn.on('disconnected', () => {
    return console.error('Disconnected from ' + dbURL);
});
process.on('SIGINT', function () {
    mongoose.connection.close(function () {
        process.exit(0);
    });
});

export default { conn };
