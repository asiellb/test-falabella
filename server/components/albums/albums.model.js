import mongoose from 'mongoose';

var AlbumSchema = mongoose.Schema({
    id: { type: String, unique: true },
    name: { type: String },
    uri: { type: String },
    href: { type: String },
    total_tracks: { type: Number },
    release_date: { type: String },
    release_date_precision: { type: String },
    images: { type: Array },
    external_urls: { type: Object },
    available_markets: { type: Array },
    artists: { type: Array },
    album_type: { type: String }
}, { timestamps: true });

export default mongoose.model('Album', AlbumSchema, 'Album');