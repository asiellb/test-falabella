import moment from 'moment';
import axios from 'axios';

class SpotifyAuth {
    constructor(client_id, client_secret) {
        this._client_id = client_id;
        this._client_secret = client_secret
    }

    getToken = async () => {
        if (isValidToken(this._current_token)) {
            return this._current_token.token_type + " " + this._current_token.access_token;
        } else {
            try {
                const token_data = await authWithClientCredentials(this._client_id, this._client_secret);
                this._current_token = token_data;
                return this._current_token.token_type + " " + this._current_token.access_token;
            } catch (error) {
                throw error;
            }
        }
    }
}

const isValidToken = (token) => {
    if (!token) return false;
    if (moment().diff(token.expires_in) < 0) return true;
    else return false;
}

const authWithClientCredentials = async (client_id, client_secret) => {
    try {
        const token = Buffer.from(client_id + ":" + client_secret).toString('base64');
        const current_date = moment();
        const { data } = await axios('https://accounts.spotify.com/api/token',
            {
                method: 'POST',
                params: { grant_type: 'client_credentials' },
                headers: {
                    Authorization: 'Basic ' + token,
                    "Content-Type": "application/x-www-form-urlencoded"
                }
            });
        data.expires_in = current_date.add(data.expires_in, 's');
        return data;
    } catch (error) {
        throw error;
    }
}

export { SpotifyAuth };